<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

$file = "/home/loomy/nuts/ruleta";
$file_ignore = "/home/loomy/nuts/ruleta-ign";

$imagefile = "fenixlogo.jpg";

$src_url = "https://gitlab.com/ToKe79/phoenix-talker-tools/";

// list of ignored players - read from file
$badguys = array();
if (($fpig = @fopen($file_ignore, "r")))
{
	$nick = trim(fgets($fpig));
	while(!feof($fpig)) {
		if (strlen($nick) != 0)
			$badguys[] = $nick;
		$nick = trim(fgets($fpig));
	}
	fclose($fpig);
}

// try to read the raw file
if(($fp = @fopen($file, "r")) === FALSE)
	die("ERROR: cannot read ruleta file!");

// body colors
$bgcolor = "black";
$fgcolor = "#5e84a5";

// table header colors
$titlebg = "#32363d";
$titlefg = "#89aae0";

// color for top guessed number
$topcolor = "#c9ba73";

// color for the nicknames
$nickcolor = "#b8c9e6";

// color for the number
$digitcolor = "#aab6e3";

// table header colors (selected)
$selectedbg = "lightgray";
$selectedfg = "darkblue";

// what actions we have
$actions = array(
	"rolled",
	"guessed",
	"won",
);

// translation for actions for the table output
$act_sk = array(
	"rolled" => "padlo",
	"guessed" => "hádané",
	"won" => "správne",
);

// empty players array - per player results will be stored here
$players = array();

// numbers array - per number results will be stored here
$numbers = array();
for($i = 0; $i < 11; $i++)
	foreach($actions as $act)
		$numbers['total'][$i][$act] = 0;

// years array - years with data will be stored here
$years = array();
$years[] = "total";

// how can we sort
$orders = array(
	"luck" => "úspešnosti",
	"wins" => "výhier",
	"name" => "prezývky",
);

// default order is by luck
if(isset($_POST['order']))
	foreach($orders as $key => $value) {
		// check if value matches one of the predefined
		if($_POST['order'] === $key)
			break;
		else
			continue;
		// does not match - set default
		$_POST['order'] = "luck";
	}
else
	$_POST['order'] = "luck";

if(!isset($_POST['year']))
	$_POST['year'] = NULL;

// read the file
$line = trim(fgets($fp));
while(!feof($fp)) {
	// skip empty lines
	if(strlen($line) == 0)
		continue;
	$data = explode(" ", $line);
	// skip lines that do not have right number members
	if(count($data) != 9)
		continue;
	/* array has these members:
	 * 0 = date - [dd.mm.year
	 * 1 = time = hour:minutes]
	 * 2 = nick
	 * 3 = string 'hada'
	 * 4 = guessed number
	 * 5 = string 'a'
	 * 6 = string 'spravne'
	 * 7 = string 'je'
	 * 8 = winning number
	 */
	$year = substr($data[0], 7, 4);
	$nick = $data[2];
	$guessed = (int)$data[4];
	$rolled = (int)$data[8];
	// is this other/ignored player?
	$other = false;
	foreach($badguys as $badguy)
		if($nick == $badguy) {
			$other = true;
			break;
		}
	// initialize array for new year
	if(!isset($numbers[$year])) {
		$years[] = $year;
		for($i = 0; $i < 11; $i++)
			foreach($actions as $act)
				$numbers[$year][$i][$act] = 0;
	}
	// create empty member for new nick
	if(!isset($players['total'][$nick]) AND $other == false) {
		$players['total'][$nick]['name'] = $nick;
		for($i = 0; $i < 11; $i++)
			foreach($actions as $act)
				$players['total'][$nick][$i][$act] = 0;
	}
	if(!isset($players[$year][$nick]) AND $other == false) {
		$players[$year][$nick]['name'] = $nick;
		for($i = 0; $i < 11; $i++)
			foreach($actions as $act)
				$players[$year][$nick][$i][$act] = 0;
	}
	// create empty array for other/ignored players
	if(!isset($others['total']) AND $other == true)
		for($i = 0; $i < 11; $i++)
			foreach($actions as $act)
				$others['total'][$i][$act] = 0;
	if(!isset($others[$year]) AND $other == true)
		for($i = 0; $i < 11; $i++)
			foreach($actions as $act)
				$others[$year][$i][$act] = 0;
	// store results
	if($other) {
		$others[$year][$guessed]['guessed']++;
		$others[$year][$rolled]['rolled']++;
		$others['total'][$guessed]['guessed']++;
		$others['total'][$rolled]['rolled']++;
		if($rolled === $guessed) {
			$others[$year][$guessed]['won']++;
			$others['total'][$guessed]['won']++;
		}
	} else {
		$players[$year][$nick][$guessed]['guessed']++;
		$players[$year][$nick][$rolled]['rolled']++;
		$players['total'][$nick][$guessed]['guessed']++;
		$players['total'][$nick][$rolled]['rolled']++;
		if ($rolled === $guessed) {
			$players[$year][$nick][$guessed]['won']++;
			$players['total'][$nick][$guessed]['won']++;
		}
	}
	$numbers[$year][$guessed]['guessed']++;
	$numbers[$year][$rolled]['rolled']++;
	$numbers['total'][$guessed]['guessed']++;
	$numbers['total'][$rolled]['rolled']++;
	if ($rolled === $guessed) {
		$numbers[$year][$guessed]['won']++;
		$numbers['total'][$guessed]['won']++;
	}
	$line = trim(fgets($fp));
}
fclose($fp);
if(count($years) > 0)
	rsort($years);
else
	die("ERROR: zero data!");
if($_POST['year'] !== NULL)
	foreach($years as $year) {
		if($_POST['year'] === $year)
			break;
		else
			continue;
		$year = $years[0];
	}
else
	$year = $years[0];
if(count($players[$year]) > 0) {
	// calculate total rolls, wins and luck
	foreach($players[$year] as $key => $player) {
		$rolls = 0;
		$wins = 0;
		for($i = 0; $i < 11; $i++) {
			$rolls+= $player[$i]['rolled'];
			$wins+= $player[$i]['won'];
		}
		$players[$year][$key]['rolls'] = $rolls;
		$players[$year][$key]['wins'] = $wins;
		$players[$year][$key]['luck'] = $wins/$rolls*100;
	}
	// sorting
	$sortby = $_POST['order'];
	$order = array();
	$oid = 0;
	$pid = count($players[$year]);
	while($oid < $pid) {
		$oldmax = 0;
		if($sortby === "name")
			$oldmax = "";
		foreach($players[$year] as $curnick => $player) {
			$already_assigned = false;
			if(count($order) > 0) {
				foreach($order as $nick) {
					if($nick === $curnick) {
						$already_assigned = true;
						break;
					}
				}
			}
			if($already_assigned)
				continue;
			$curmax = $player[$sortby];
			if($curmax >= $oldmax) {
				$foundnick = $curnick;
				$oldmax = $curmax;
			}
		}
		$order[] = $foundnick;
		$oid++;
	}
	// if we sort by name, reverse the order
	if($sortby === "name")
		sort($order);
	// get most guessed/rolled/winning numbers
	foreach($actions as $act) {
		// per player
		foreach($order as $nick) {
			$topnumbers[$act][$nick] = array();
			$sorted = 0;
			while($sorted < 11) {
				$oldmax = 0;
				for($i = 0; $i < 11; $i++) {
					$curmax = $players[$year][$nick][$i][$act];
					$already_assigned = false;
					if(count($topnumbers[$act][$nick]) > 0) {
						foreach($topnumbers[$act][$nick] as $digit) {
							if($digit === $i) {
								$already_assigned = true;
								break;
							}
						}
					}
					if($already_assigned)
						continue;
					if($curmax >= $oldmax) {
						$found = $i;
						$oldmax = $curmax;
					}
				}
				$topnumbers[$act][$nick][] = $found;
				$sorted++;
			}
		}
		// overall
		$topnumbers[$act]['all_players'] = array();
		$sorted = 0;
		while($sorted < 11) {
			$oldmax = 0;
			for($i = 0; $i < 11; $i++) {
				$curmax = $numbers[$year][$i][$act];
				$already_assigned = false;
				if(count($topnumbers[$act]['all_players']) > 0) {
					foreach($topnumbers[$act]['all_players'] as $digit) {
						if($digit === $i) {
							$already_assigned = true;
							break;
						}
					}
				}
				if($already_assigned)
					continue;
				if($curmax >= $oldmax) {
					$found = $i;
					$oldmax = $curmax;
				}
			}
			$topnumbers[$act]['all_players'][] = $found;
			$sorted++;
		}
	}
	// output
	print '<!DOCTYPE html>
<html lang="sk">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Phoenix :: Statistiky rulety</title>
<style>
body {
	background-color: '.$bgcolor.';
	color: '.$fgcolor.';
	margin: 0;
	padding: 2rem;
	text-align: center;
	display: block;
}

table {
	position: relative;
	border-collapse: collapse;
	margin-left: auto;
	margin-right: auto;
}


th, td {
	padding: 0.25rem;
	border: 1px solid '.$fgcolor.';
	text-align: center;
}

th {
	background: '.$titlebg.';
	color: '.$titlefg.';
}

th.title {
    position: sticky;
    top: -1px;
}

th.selected {
	background: '.$selectedbg.';
	color: '.$selectedfg.';
}

th.blank {
	border: none;
	background: '.$bgcolor.';
	color: '.$fgcolor.';
}

td.nick {
    font-weight: bold;
    text-align: left;
    color: '.$nickcolor.';
}

td.digit {
    text-align: right;
}

tr.total {
    font-weight: bold;
}

.topnumber {
    font-weight: bold;
    color: '.$topcolor.';
}

a:link {
	color: #667296;
}

a:visited {
	color: #667296;
}

a:hover {
	color: #667296;
}

.cursor_pointer {
	cursor: pointer;
}
</style>
<script>
function sorttable(by) {
	form = document.getElementById(\'sortform\');
	document.getElementById(\'sortselect\').value = by;
	form.submit();
}

function selectyear(yr) {
	form = document.getElementById(\'sortform\');
	document.getElementById(\'yearselect\').value = yr;
	form.submit();
}
</script>
</head>
<body>
<img src="'.$imagefile.'" alt="Phoenix Talker logo">
<h1>Phoenix - štatistiky rulety</h1>
<form id="sortform" method="POST">
<input type="hidden" id="sortselect" name="order" value="'.$sortby.'" />
<input type="hidden" id="yearselect" name="year" value="'.$year.'" />
</form>
<table>
<tr>
';
	foreach($years as $value) {
		if($value === "total")
			$out = "všetky roky";
		else
			$out = $value;
		print '<th class="'.($year == $value ? 'selected' : 'cursor_pointer').'"'.($year != $value ? ' onclick="selectyear(\''.$value.'\');"' : '').'>'.$out.'</th>
';
	}
	print '<th class="blank">&nbsp;&nbsp;</th>
';
	foreach($orders as $key => $value) {
		print '<th class="'.($sortby == $key ? 'selected' : 'cursor_pointer').'"'.($sortby != $key ? ' onclick="sorttable(\''.$key.'\');"' : '').'>podľa '.$value.'</th>
';
	}
print '</tr>
</table>
<br />
<table>
<tr>
<th class="title">Por.</th>
<th class="title" colspan="2">Prezývka</th>';

	for($i = 0; $i < 11; $i++)
		print '<th class="title">'.$i.'</th>
';
	print '<th class="title">Spolu</th>
</tr>';
	$pos = 0;
	foreach($order as $nick) {
		print '<tr>
<td rowspan="3">'.++$pos.'.</td>
<td class="nick" rowspan="3">'.$players[$year][$nick]['name'].'</td>
';
		foreach($actions as $act) {
			if($act !== "rolled")
				print '<tr>
';
			print '<td>'.$act_sk[$act].'</td>
';
			for($i = 0; $i < 11; $i++) {
				$opendiv = '';
				$closediv = '';
				if($topnumbers[$act][$nick][0] === $i) {
					$opendiv = '<div class="topnumber">';
					$closediv = '</div>';
				}
				print '<td class="digit">'.$opendiv._number_format($players[$year][$nick][$i][$act], 0).$closediv.'</td>
';
			}
			if($act === "rolled")
				print '<td rowspan="3">'._number_format($players[$year][$nick]['rolls'], 0).' '.sklonuj($players[$year][$nick]['rolls'], "točenie", "točenia", "točení").'<br />'._number_format($players[$year][$nick]['wins'], 0).' '.sklonuj($players[$year][$nick]['wins'], "výhra", "výhry", "výhier").'<br />úspešnosť: '._number_format($players[$year][$nick]['luck'], 2).' %</td>
';
			print '</tr>
';
		}
	}
	if(isset($others[$year])) {
		print '<tr>
<td rowspan="3" colspan="2">Ostatní</td>';
		foreach($actions as $act) {
			$rolls = 0;
			$wins = 0;
			for($i = 0; $i < 11; $i++) {
				$rolls+= $others[$year][$i]['rolled'];
				$wins+= $others[$year][$i]['won'];
			}
			if($act !== "rolled")
				print '
<tr>';
			print '<td>'.$act_sk[$act].'</td>
';
			for($i = 0; $i < 11; $i++)
				print '<td class="digit">'._number_format($others[$year][$i][$act], 0).'</td>
';
			if($act === "rolled")
				print '<td rowspan="3">'._number_format($rolls, 0).' '.sklonuj($rolls, "točenie", "točenia", "točení").'<br />'._number_format($wins, 0).' '.sklonuj($wins, "výhra", "výhry", "výhier").'<br />úspešnosť: '._number_format($wins/$rolls*100, 2).' %</td>
';
			print '</tr>
';
		}
	}
	print '<tr class="total">
<td rowspan="3" colspan="2">Celkom</td>
';
	foreach($actions as $act) {
		$rolls = 0;
		$wins = 0;
		for($i = 0; $i < 11; $i++) {
			$rolls+= $numbers[$year][$i]['rolled'];
			$wins+= $numbers[$year][$i]['won'];
		}
		if($act !== "rolled")
			print '<tr class="total">
';
		print '<td>'.$act_sk[$act].'</td>
';
		for($i = 0; $i < 11; $i++)
			print '<td class="digit">'._number_format($numbers[$year][$i][$act], 0).'</td>
';
		if($act === "rolled")
			print '<td rowspan="3">'._number_format($rolls, 0).' '.sklonuj($rolls, "točenie", "točenia", "točení").'<br />'._number_format($wins, 0).' '.sklonuj($wins, "výhra", "výhry", "výhier").'<br />úspešnosť: '._number_format($wins/$rolls*100, 2).' %</td>
';
		print '</tr>
';
	}
	print '<tr>
<th class="title" colspan="3"></th>';

	for($i = 0; $i < 11; $i++)
		print '<th class="title">'.$i.'</th>
';
		print '<th class="title">Spolu</th>
</tr>
</table>
<h2>Ako často je ktoré číslo hádané</h2>
<table>
';
	// sort nicks alphabetically
	sort($order);
	foreach($order as $nick) {
		print '<tr>
<th>'.$nick.'</th>';
		foreach($topnumbers['guessed'][$nick] as $digit)
			print '<td><span style="color: '.$digitcolor.'">'.$digit.'</span> ('._number_format($players[$year][$nick][$digit]['guessed'], 0).'x)</td>
';
		print '</tr>
';
	}
	print '</table>
<br />
<h2>Najčastejšie hádane čísla (celkovo)</h2>
<table>
';
	foreach($topnumbers['guessed']['all_players'] as $digit) {
		$percentage = _number_format($numbers[$year][$digit]['guessed'] / $rolls * 100, 2);
		print '<tr>
<th>'.$digit.'</th>
<td>'._number_format($numbers[$year][$digit]['guessed'], 0).'x</td>
<td>'.$percentage.' %</td>
</tr>
';
	}
	print '</table>
<br />
<h2>Ako často ktoré číslo padá</h2>
<table>
';
	foreach($order as $nick) {
		print '<tr>
<th>'.$nick.'</th>
';
		foreach($topnumbers['rolled'][$nick] as $digit)
			print '<td><span style="color: '.$digitcolor.'">'.$digit.'</span> ('._number_format($players[$year][$nick][$digit]['rolled'], 0).'x)</td>
';
		print '</tr>
';
	}
	print '</table>
<br />
<h2>Najčastejšie padajúce čísla (celkovo)</h2>
<table>
';
	foreach($topnumbers['rolled']['all_players'] as $digit) {
		$percentage = _number_format($numbers[$year][$digit]['rolled'] / $rolls * 100, 2);
		print '<tr>
<th>'.$digit.'</th>
<td>'._number_format($numbers[$year][$digit]['rolled'], 0).'x</td>
<td>'.$percentage.' %</td>
</tr>
';
	}
	print '</table>
<br />
<a href="./">Spät</a>
<br /><br />
<a href="'.$src_url.'" target="_blank">Štatistiku pre Phoenix talker nakódil Voodoo</a>
</body>
</html>';
}

function _number_format($value, $prec) {
	return number_format(round($value, $prec), $prec, ",", "&nbsp;");
}

function sklonuj($number, $singular, $plural2, $plural5) {
	if($number === 1)
		return $singular;
	elseif($number > 0 AND $number < 5)
		return $plural2;
	else
		return $plural5;
}
