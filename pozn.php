<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.
?>

<?php
$meno = "";
$heslo = "";
$profil = "";
$action = "";
$message = "Nespravny login";
$path = "/home/loomy/nuts/userfiles";

if(isset($_POST['meno'])) $meno = $_POST['meno'];
if(isset($_POST['heslo'])) $heslo = $_POST['heslo'];
if(isset($_POST['profil'])) $profil = $_POST['profil'];
if(isset($_POST['action'])) $action = $_POST['action'];

function profileform($message, $meno, $heslo, $profil) 
{
 print '<html>
 <head>
 <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
 <title>Phoenix :: Poznamkovy editor</title>
 </head>
 <body text=#5e84a5 bgcolor=#2b447e>
 <center><h2>Phoenix Poznamkovy editor</h2>
 <p>' . $message . '</p>
 <form method="post">
 <textarea name="profil" maxlength="9000" rows="30" cols="80">' . $profil . '</textarea><br>
 <input type="text" name="meno" placeholder="Meno" value="' . $meno . '" maxlenght="12"><br>
 <input type="password" name="heslo" placeholder="Heslo" value="' . $heslo . '" maxlength="20"><br>
 <input type="hidden" name="action" value="save_profile">
 <input type="submit" value="Uloz">
 </form>
 <a href="https://loomy.wheel.sk/fenix/">Spat</a>
 </body>
 </html>';
}

function intro($message, $meno, $heslo)
{
 print '<html>
 <head>
 <meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
 <title>Phoenix :: Poznamkovy editor</title>
 </head>
 <body text=#5e84a5 bgcolor=#2b447e>
 <center><h2>Phoenix poznamkovy editor</h2>
 <p>' . $message . '</p>
 <form method="post">
 <input type="text" name="meno" placeholder="Meno" value="' . $meno . '" maxlenght="12"><br>
 <input type="password" name="heslo" placeholder="Heslo" value="' . $heslo . '" maxlength="20"><br>
 <input type="hidden" name="action" value="load_profile">
 <input type="submit" value="Prihlas">
 </form>
 <a href="https://loomy.wheel.sk/fenix/">Spat</a>
 </body>
 </html>';
}

function checkpwd ($heslo, $hash)
{
 if (strlen($hash)<15) 
  { 
   if (strcmp($hash,crypt($heslo,'NU')) == 0) return 0;
   else return 1;
  }
 else
  {
   $prvychosem = substr($heslo,0,8); 
   $druhychosem = substr($heslo,8,8);
   $dokopy = crypt($prvychosem,'NU') + crypt($druhychosem,'NU');
   if ($dokopy == $hash) return 0;
   else return 1;
  }
}

if ($action == "save_profile") 
 { if ($meno != "" && $heslo != "" && $profil != "")  
    {
     if (substr_count($profil, "\n") < 5000) 
      {
	$nick = strtoupper($meno[0]).substr($meno,1);
	$file = $path . "/" . $nick . ".D";
	if (file_exists($file)) 
         {
          $f = fopen($file, 'r');
          $line = fgets($f);
          fclose($f);
          $l = str_replace("\n", "", $line);
          if (checkpwd($heslo,$l) == 0) 
           {
	    if (substr($profil, -1) != "\n") $profil.= "\n"; 
	    $file = $path . "/" . $nick . ".poz";
	    $fp = fopen($file, "w") or die("ERROR: could not open file!");
	    fwrite($fp, $profil) or die("ERROR: could not write to file!");
	    fclose($fp);
	    $save = "success";
	   } 
          else { $save = "wrongPass"; }
         }
	else { $save = "wrongPass"; }
      } 
     else { $save = "tooManyLines"; }
     switch($save) {
     case "success": $message = "Tvoje poznamky boli ulozene."; 	break;
     case "tooManyLines": $message = "Poznamky mozu mat maximalne 5000 riadkov!"; break;
     case "wrongPass": $message = "Nespravne meno alebo heslo!"; 	break; }
    } 
  else { $message = "Na ulozenie poznamok treba zadat meno, heslo a zadat poznamky!"; }
  profileform($message, $meno, $heslo, $profil);
  die();
 }
else 
  if ( $action == "load_profile" )  
    {
     if ($meno != "" && $heslo != "") 
      {
       if (strlen($meno) < 3)  { $load = "nickTooShort"; }
       else 
        {
	 $nick = strtoupper($meno[0]).substr($meno,1);
	 $file = $path . "/" . $nick . ".D";
	 if (file_exists($file)) 
          {
           $f = fopen($file, 'r');
       	   $line = fgets($f);
       	   fclose($f);
       	   $l = str_replace("\n", "", $line);
       	   if (checkpwd($heslo,$l) == 0) 
            { 
	     $file = $path . "/" . $nick . ".poz";
	     if (file_exists($file)) { $profil = file_get_contents($file); } 
             else { $profil = "(Ziadne poznamky)"; }
	     $load = "success";
	    } 
          } else  { $load = "wrongPass"; } 
        } 
      } 
     else  { $load = "wrongPass"; }
     switch($load) 
      {
       case "success": $message = "Poznamky nacitane."; break;
       case "nickTooShort": $message = "Prilis kratky nick."; break;
       case "wrongPass": $message = "Nespravne meno alebo heslo!"; break;
      }
     if ($load != "success") { intro($message, $meno, $heslo); }
     else { profileform($message, $meno, $heslo, $profil); }
     die();
    } 
   else { $message = "Prihlasenie tu ta neodhlasi z talkera."; }

intro($message, $meno, $heslo);

?>
