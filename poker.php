<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

function posnegcolor($value) {
	if($value > 0)
		return "green";
	else
		return "red";
}

function nrfrmt($value, $prec) {
	return number_format(round($value, $prec), $prec, ",", "&nbsp;");
}

// body colors
$bgcolor = "black";
$fgcolor = "#5e84a5";

// table header colors
$titlebg = "#32363d";
$titlefg = "#89aae0";

// table header colors (sorted column)
$sorttitlebg = "lightgray";
$sorttitlefg = "darkblue";

// color for the nicknames
$nickcolor = "#b8c9e6";

$imagefile = "fenixlogo.jpg";

$src_url = "https://gitlab.com/ToKe79/phoenix-talker-tools/";

$file = "/home/loomy/nuts/kphry";

/* example of one line in the file:

0      1       2      4  4       5      6       7 8      9       10     11  12    13     14   15          16
1      2       3      4  5       6      7       8 9      10      11     12  13    14     15   16          17
Vitaz: Kacicka vyhra: 30 (skore: 11000) hracov: 2 Hrali: Kacicka vklad: 60, Roger vklad: 30,  (24.12.2019 12:59)

in element [7] we have number of players, so a valid line must have at least 8 elements.
After we check that the line has at least 8 elements, the total number of elements must be:
8 [first eight elements] + 1 [string 'Hrali:'] + number of players * 3 [each player has name + string 'vklad:' + amount of bet] + 1 [extra space before date and time] + 2 [strings for date and time]
or simplified: 12 + 3*nr_of_players

we also remove brackets and commas from the string, after explode the array is:
[0] - string (we skip lines where this string is is 'Leavol'
[1] - name of the winner
[2] - string, ignored
[3] - amount in pot
[4] - string, ignored
[5] - score of the winning player
[6] - string - ignored
[7] - number of players
[8] - string, ignored
[6+n*3] - name of player n (where n in 1 to value of [7])
[7+n*3] - string, ignored
[8+n*3] - bet of player n
[9+X*3] - blank (X = value of [7])
[10+X*3] - date (j.n.Y)
[11+X*3] - time [G:i]

 */

$ignored = array(
	"Hopko",
	"Lalja",
);
// minimum elements
define("ME", 8);

if(($fp = @fopen($file, "r")) === FALSE)
	die("Sorry, cannot access data!");

$linenr = 0;
$msgout = "";
$warnings = 0;
$badchars = array(
	",",
	"(",
	")",
);

$games = array();
$playerdata = array();
$lws = array();
$datafrom = "";
$datauntil = "";
$years = array();
$years[] = 'total';
$sortorders = array(
	"wins",
	"lost",
	"games",
	"topscore",
	"wingold",
	"lostgold",
	"netgold",
	"topwin",
	"topbet",
	"bets",
	"name",
	"lws_highest",
);

$tabletitles = array(
	"name" => "Prezývka",
	"wins" => "Výhry",
	"lost" => "Prehry",
	"games" => "Počet hier",
	"topscore" => "Najvyššie víťazné skóre",
	"wingold" => "Vyhratý gold",
	"lostgold" => "Prehratý gold",
	"netgold" => "Zisk / strata",
	"topwin" => "Najvyššia výhra",
	"topbet" => "Najvyššia stávka",
	"bets" => "Suma stávok",
	"lws_highest" => "Séria výhier",
);

$monthnames = array(
	"Január",
	"Február",
	"Marec",
	"Apríl",
	"Máj",
	"Jún",
	"Júl",
	"August",
	"September",
	"Október",
	"November",
	"December",
);

$line = trim(fgets($fp));
while(!feof($fp)) {
	// remove double spaces, commas and brackets
	$line = str_replace($badchars, '', $line);
	$linenr++;
	//echo "Processing line ".$linenr.": ".$line."\n";
	if(strlen($line) == 0) {
		$line = trim(fgets($fp));
		continue;
	}
	$data = explode(" ", $line);
	if($data[0] == "Leavol") {
		$msgout.= "Skipped line #".$linenr.": '".$line."'\n";
		$line = trim(fgets($fp));
		continue;
	}
	if(count($data) < ME) {
		$msgout.= "Cannot process line #".$linenr." - less than ".ME." elements: '".$line."'\n";
		$warnings++;
		$line = trim(fgets($fp));
		continue;
	}
	$players = (int)$data[ME-1];
	// expected elements
	$ee = 12 + $players*3;
	if(count($data) != $ee) {
		$msgout.= "Cannot process line #".$linenr." - unexpected count of elements (".count($data)." instead of ".$ee."): '".$line."'\n";
		$warnings++;
		$line = trim(fgets($fp));
		continue;
	}
	$skip = false;
	foreach($ignored as $ign) {
		if(strpos($line, $ign) !== false) {
			$skip = true;
			break;
		}
	}
	if($skip) {
		$msgout.= "Skipped line #".$linenr." - contains ignored player '".$ign."': '".$line."'\n";
		$warnings++;
		$line = trim(fgets($fp));
		continue;
	}
	$date = $data[10+$players*3];
	$time = $data[11+$players*3];
	$datearr = explode(".", $date);
	$year = $datearr[2];
	$month = $datearr[1];
	if(!isset($games[$year])) {
		$years[] = $year;
		$games[$year]['count'] = 0;
		$games[$year]['gold'] = 0;
		$games[$year]['topscore'] = 0;
		$games[$year]['topscoreplayer'] = "";
		$games[$year]['maxplayers'] = 0;
		$games[$year]['topbet'] = 0;
		$games[$year]['topbetplayers'] = array();
		$games[$year]['topwin'] = 0;
		$games[$year]['topwinplayers'] = array();
		$games[$year]['months'] = array();
		$games[$year]['highestpot']['pot'] = 0;
		$games[$year]['highestpot']['date'] = 0;
		$games[$year]['highestpot']['time'] = 0;
		$games[$year]['highestpot']['players'] = array();
		$games[$year]['highestpot']['winner'] = "";
		$games[$year]['highestpot']['win'] = 0;
	}
	if(!isset($games[$year]['months'][$month])) {
		$games[$year]['months'][$month]['count'] = 0;
		$games[$year]['months'][$month]['gold'] = 0;
		$games[$year]['months'][$month]['topscore'] = 0;
		$games[$year]['months'][$month]['topscoreplayer'] = "";
		$games[$year]['months'][$month]['maxplayers'] = 0;
		$games[$year]['months'][$month]['topbet'] = 0;
		$games[$year]['months'][$month]['topbetplayers'] = array();
		$games[$year]['months'][$month]['topwin'] = 0;
		$games[$year]['months'][$month]['topwinplayers'] = array();
		$games[$year]['months'][$month]['highestpot']['pot'] = 0;
		$games[$year]['months'][$month]['highestpot']['date'] = 0;
		$games[$year]['months'][$month]['highestpot']['time'] = 0;
		$games[$year]['months'][$month]['highestpot']['players'] = array();
		$games[$year]['months'][$month]['highestpot']['winner'] = "";
		$games[$year]['months'][$month]['highestpot']['win'] = 0;
	}
	if(!isset($games['total'])) {
		$games['total']['count'] = 0;
		$games['total']['gold'] = 0;
		$games['total']['topscore'] = 0;
		$games['total']['topscoreplayer'] = "";
		$games['total']['maxplayers'] = 0;
		$games['total']['topbet'] = 0;
		$games['total']['topbetplayers'] = array();
		$games['total']['topwin'] = 0;
		$games['total']['topwinplayers'] = array();
		$games['total']['highestpot']['pot'] = 0;
		$games['total']['highestpot']['date'] = 0;
		$games['total']['highestpot']['time'] = 0;
		$games['total']['highestpot']['players'] = array();
		$games['total']['highestpot']['winner'] = "";
		$games['total']['highestpot']['win'] = 0;
	}
	$games[$year]['count']++;
	$games[$year]['months'][$month]['count']++;
	$games['total']['count']++;
	if($players > $games[$year]['maxplayers'])
		$games[$year]['maxplayers'] = $players;
	if($players > $games[$year]['months'][$month]['maxplayers'])
		$games[$year]['months'][$month]['maxplayers'] = $players;
	if($players > $games['total']['maxplayers'])
		$games['total']['maxplayers'] = $players;
	if($datafrom == "")
		$datafrom = $date." ".$time;
	$datauntil = $date." ".$time;
	$winner = $data[1];
	$win = (int)$data[3];
	$score = (int)$data[5];
	if($score > $games[$year]['topscore']) {
		$games[$year]['topscore'] = $score;
		$games[$year]['topscoreplayer'] = $winner;
	}
	if($score > $games[$year]['months'][$month]['topscore']) {
		$games[$year]['months'][$month]['topscore'] = $score;
		$games[$year]['months'][$month]['topscoreplayer'] = $winner;
	}
	if($score > $games['total']['topscore']) {
		$games['total']['topscore'] = $score;
		$games['total']['topscoreplayer'] = $winner;
	}
	$playerlist = array();
	$pot = 0;
	for($i = 1; $i <= $players; $i++) {
		$playername = $data[6+$i*3];
		if($playername == "Derae")
			$playername = "Niki";
		$bet = (int)$data[8+$i*3];
		$playerlist[$playername] = $bet;
		$pot+= $bet;
		$games[$year]['gold']+= $bet;
		$games[$year]['months'][$month]['gold']+= $bet;
		$games['total']['gold']+= $bet;
		if($bet >= $games[$year]['topbet']) {
			if($bet == $games[$year]['topbet']) {
				$betfoundname = false;
				foreach($games[$year]['topbetplayers'] as $name) {
					if($playername === $name) {
						$betfoundname = true;
						break;
					}
				}
				if(!$betfoundname)
					$games[$year]['topbetplayers'][] = $playername;
			} else {
				$games[$year]['topbet'] = $bet;
				$games[$year]['topbetplayers'] = array();
				$games[$year]['topbetplayers'][] = $playername;
			}
		}
		if($bet >= $games[$year]['months'][$month]['topbet']) {
			if($bet == $games[$year]['months'][$month]['topbet']) {
				$betfoundname = false;
				foreach($games[$year]['months'][$month]['topbetplayers'] as $name) {
					if($playername === $name) {
						$betfoundname = true;
						break;
					}
				}
				if(!$betfoundname)
					$games[$year]['months'][$month]['topbetplayers'][] = $playername;
			} else {
				$games[$year]['months'][$month]['topbet'] = $bet;
				$games[$year]['months'][$month]['topbetplayers'] = array();
				$games[$year]['months'][$month]['topbetplayers'][] = $playername;
			}
		}
		if($bet >= $games['total']['topbet']) {
			if($bet == $games['total']['topbet']) {
				$betfoundname = false;
				foreach($games['total']['topbetplayers'] as $name) {
					if($playername === $name) {
						$betfoundname = true;
						break;
					}
				}
				if(!$betfoundname)
					$games['total']['topbetplayers'][] = $playername;
			} else {
				$games['total']['topbet'] = $bet;
				$games['total']['topbetplayers'] = array();
				$games['total']['topbetplayers'][] = $playername;
			}
		}
		if($win >= $games[$year]['topwin'] AND $playername == $winner) {
			if($win == $games[$year]['topwin']) {
				$winfoundname = false;
				foreach($games[$year]['topwinplayers'] as $name) {
					if($playername === $name) {
						$winfoundname = true;
						break;
					}
				}
				if(!$winfoundname)
					$games[$year]['topwinplayers'][] = $playername;
			} else {
				$games[$year]['topwin'] = $win;
				$games[$year]['topwinplayers'] = array();
				$games[$year]['topwinplayers'][] = $playername;
			}
		}
		if($win >= $games[$year]['months'][$month]['topwin'] AND $playername == $winner) {
			if($win == $games[$year]['months'][$month]['topwin']) {
				$winfoundname = false;
				foreach($games[$year]['months'][$month]['topwinplayers'] as $name) {
					if($playername === $name) {
						$winfoundname = true;
						break;
					}
				}
				if(!$winfoundname)
					$games[$year]['months'][$month]['topwinplayers'][] = $playername;
			} else {
				$games[$year]['months'][$month]['topwin'] = $win;
				$games[$year]['months'][$month]['topwinplayers'] = array();
				$games[$year]['months'][$month]['topwinplayers'][] = $playername;
			}
		}
		if($win >= $games['total']['topwin'] AND $playername == $winner) {
			if($win == $games['total']['topwin']) {
				$winfoundname = false;
				foreach($games['total']['topwinplayers'] as $name) {
					if($playername === $name) {
						$winfoundname = true;
						break;
					}
				}
				if(!$winfoundname)
					$games['total']['topwinplayers'][] = $playername;
			} else {
				$games['total']['topwin'] = $win;
				$games['total']['topwinplayers'] = array();
				$games['total']['topwinplayers'][] = $playername;
			}
		}
		if(!isset($playerdata[$year][$playername])) {
			$playerdata[$year][$playername]['name'] = $playername;
			$playerdata[$year][$playername]['games'] = 0;
			$playerdata[$year][$playername]['bets'] = 0;
			$playerdata[$year][$playername]['wingold'] = 0;
			$playerdata[$year][$playername]['wins'] = 0;
			$playerdata[$year][$playername]['topscore'] = 0;
			$playerdata[$year][$playername]['topbet'] = 0;
			$playerdata[$year][$playername]['topwin'] = 0;
			$playerdata[$year][$playername]['lost'] = 0;
			$playerdata[$year][$playername]['lostgold'] = 0;
			$playerdata[$year][$playername]['netgold'] = 0;
			$playerdata[$year][$playername]['lws_current'] = 0;
			$playerdata[$year][$playername]['lws_highest'] = 0;
			$playerdata[$year][$playername]['lws_wonlastgame'] = false;
		}
		if(!isset($playerdata[$year][$playername]['multi'][$players])) {
			$playerdata[$year][$playername]['multi'][$players]['played'] = 0;
			$playerdata[$year][$playername]['multi'][$players]['won'] = 0;
			$playerdata[$year][$playername]['multi'][$players]['lost'] = 0;
			$playerdata[$year][$playername]['multi'][$players]['goldwon'] = 0;
			$playerdata[$year][$playername]['multi'][$players]['goldlost'] = 0;
			$playerdata[$year][$playername]['multi'][$players]['goldnet'] = 0;
		}
		if(!isset($playerdata['total'][$playername])) {
			$playerdata['total'][$playername]['name'] = $playername;
			$playerdata['total'][$playername]['games'] = 0;
			$playerdata['total'][$playername]['bets'] = 0;
			$playerdata['total'][$playername]['wingold'] = 0;
			$playerdata['total'][$playername]['wins'] = 0;
			$playerdata['total'][$playername]['topscore'] = 0;
			$playerdata['total'][$playername]['topbet'] = 0;
			$playerdata['total'][$playername]['topwin'] = 0;
			$playerdata['total'][$playername]['lost'] = 0;
			$playerdata['total'][$playername]['lostgold'] = 0;
			$playerdata['total'][$playername]['netgold'] = 0;
			$playerdata['total'][$playername]['lws_current'] = 0;
			$playerdata['total'][$playername]['lws_highest'] = 0;
			$playerdata['total'][$playername]['lws_wonlastgame'] = false;
		}
		if(!isset($playerdata['total'][$playername]['multi'][$players])) {
			$playerdata['total'][$playername]['multi'][$players]['played'] = 0;
			$playerdata['total'][$playername]['multi'][$players]['won'] = 0;
			$playerdata['total'][$playername]['multi'][$players]['lost'] = 0;
			$playerdata['total'][$playername]['multi'][$players]['goldwon'] = 0;
			$playerdata['total'][$playername]['multi'][$players]['goldlost'] = 0;
			$playerdata['total'][$playername]['multi'][$players]['goldnet'] = 0;
		}
		$playerdata[$year][$playername]['games']++;
		$playerdata[$year][$playername]['bets']+= $bet;
		$playerdata[$year][$playername]['multi'][$players]['played']++;
		$playerdata['total'][$playername]['games']++;
		$playerdata['total'][$playername]['bets']+= $bet;
		$playerdata['total'][$playername]['multi'][$players]['played']++;
		if($winner === $playername) {
			$playerdata[$year][$playername]['wingold']+= $win;
			$playerdata[$year][$playername]['netgold']+= $win;
			$playerdata[$year][$playername]['wins']++;
			$playerdata[$year][$playername]['multi'][$players]['won']++;
			$playerdata[$year][$playername]['multi'][$players]['goldwon']+= $win;
			$playerdata[$year][$playername]['multi'][$players]['goldnet']+= $win;
			$playerdata['total'][$playername]['wingold']+= $win;
			$playerdata['total'][$playername]['netgold']+= $win;
			$playerdata['total'][$playername]['wins']++;
			$playerdata['total'][$playername]['multi'][$players]['won']++;
			$playerdata['total'][$playername]['multi'][$players]['goldwon']+= $win;
			$playerdata['total'][$playername]['multi'][$players]['goldnet']+= $win;
			if($score > $playerdata[$year][$playername]['topscore'])
				$playerdata[$year][$playername]['topscore'] = $score;
			if($score > $playerdata['total'][$playername]['topscore'])
				$playerdata['total'][$playername]['topscore'] = $score;
			if($win > $playerdata[$year][$playername]['topwin'])
				$playerdata[$year][$playername]['topwin'] = $win;
			if($win > $playerdata['total'][$playername]['topwin'])
				$playerdata['total'][$playername]['topwin'] = $win;
		} else {
			$playerdata[$year][$playername]['lost']++;
			$playerdata['total'][$playername]['lost']++;
			$playerdata[$year][$playername]['multi'][$players]['lost']++;
			$playerdata[$year][$playername]['multi'][$players]['goldlost']+= $bet;
			$playerdata[$year][$playername]['multi'][$players]['goldnet']-= $bet;
			$playerdata['total'][$playername]['multi'][$players]['lost']++;
			$playerdata['total'][$playername]['multi'][$players]['goldlost']+= $bet;
			$playerdata['total'][$playername]['multi'][$players]['goldnet']-= $bet;
			$playerdata[$year][$playername]['lostgold']+= $bet;
			$playerdata['total'][$playername]['lostgold']+= $bet;
			$playerdata[$year][$playername]['netgold']-= $bet;
			$playerdata['total'][$playername]['netgold']-= $bet;
		}
		if($bet > $playerdata[$year][$playername]['topbet'])
			$playerdata[$year][$playername]['topbet'] = $bet;
		if($bet > $playerdata['total'][$playername]['topbet'])
			$playerdata['total'][$playername]['topbet'] = $bet;
	}
	$hp_year = false;
	if($pot > $games[$year]['highestpot']['pot']) {
		$hp_year = true;
		$games[$year]['highestpot']['pot'] = $pot;
		$games[$year]['highestpot']['date'] = $date;
		$games[$year]['highestpot']['time'] = $time;
		$games[$year]['highestpot']['players'] = array();
		$games[$year]['highestpot']['winner'] = $winner;
		$games[$year]['highestpot']['win'] = $win;
	}
	$hp_month = false;
	if($pot > $games[$year]['months'][$month]['highestpot']['pot']) {
		$hp_month = true;
		$games[$year]['months'][$month]['highestpot']['pot'] = $pot;
		$games[$year]['months'][$month]['highestpot']['date'] = $date;
		$games[$year]['months'][$month]['highestpot']['time'] = $time;
		$games[$year]['months'][$month]['highestpot']['players'] = array();
		$games[$year]['months'][$month]['highestpot']['winner'] = $winner;
		$games[$year]['months'][$month]['highestpot']['win'] = $win;
	}
	$hp_total = false;
	if($pot > $games['total']['highestpot']['pot']) {
		$hp_total = true;
		$games['total']['highestpot']['pot'] = $pot;
		$games['total']['highestpot']['date'] = $date;
		$games['total']['highestpot']['time'] = $time;
		$games['total']['highestpot']['players'] = array();
		$games['total']['highestpot']['winner'] = $winner;
		$games['total']['highestpot']['win'] = $win;
	}
	asort($playerlist);
	foreach($playerlist as $plname => $bet) {
		if($hp_year)
			$games[$year]['highestpot']['players'][$plname] = $bet;
		if($hp_month)
			$games[$year]['months'][$month]['highestpot']['players'][$plname] = $bet;
		if($hp_total)
			$games['total']['highestpot']['players'][$plname] = $bet;
		$others = array();
		foreach($playerlist as $other => $tmp)
			if($other != $plname)
				$others[] = $other;
		$others = implode("|", $others);
		if(!isset($playerdata[$year][$plname]['groups'][$players][$others])) {
			$playerdata[$year][$plname]['groups'][$players][$others]['plays'] = 0;
			$playerdata[$year][$plname]['groups'][$players][$others]['wins'] = 0;
			$playerdata[$year][$plname]['groups'][$players][$others]['lost'] = 0;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldwon'] = 0;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldlost'] = 0;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldnet'] = 0;
		}
		if(!isset($playerdata['total'][$plname]['groups'][$players][$others])) {
			$playerdata['total'][$plname]['groups'][$players][$others]['plays'] = 0;
			$playerdata['total'][$plname]['groups'][$players][$others]['wins'] = 0;
			$playerdata['total'][$plname]['groups'][$players][$others]['lost'] = 0;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldwon'] = 0;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldlost'] = 0;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldnet'] = 0;
		}
		$playerdata[$year][$plname]['groups'][$players][$others]['plays']++;
		$playerdata['total'][$plname]['groups'][$players][$others]['plays']++;
		if($plname === $winner) {
			$playerdata[$year][$plname]['groups'][$players][$others]['wins']++;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldwon']+= $win;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldnet']+= $win;
			$playerdata['total'][$plname]['groups'][$players][$others]['wins']++;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldwon']+= $win;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldnet']+= $win;
		} else {
			$playerdata[$year][$plname]['groups'][$players][$others]['lost']++;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldlost']+= $bet;
			$playerdata[$year][$plname]['groups'][$players][$others]['goldnet']-= $bet;
			$playerdata['total'][$plname]['groups'][$players][$others]['lost']++;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldlost']+= $bet;
			$playerdata['total'][$plname]['groups'][$players][$others]['goldnet']-= $bet;
		}
	}
	foreach($playerdata[$year] as $oldwinner => $windata) {
		if($winner == $oldwinner) {
			$playerdata[$year][$oldwinner]['lws_current']++;
			$playerdata[$year][$oldwinner]['lws_wonlastgame'] = true;
		} else {
			if($windata['lws_wonlastgame']) {
				if($windata['lws_current'] > $windata['lws_highest']) {
					$playerdata[$year][$oldwinner]['lws_highest'] = $windata['lws_current'];
				}
				$playerdata[$year][$oldwinner]['lws_current'] = 0;
				$playerdata[$year][$oldwinner]['lws_wonlastgame'] = false;
			}
		}
	}
	foreach($playerdata['total'] as $oldwinner => $windata) {
		if($winner == $oldwinner) {
			$playerdata['total'][$oldwinner]['lws_current']++;
			$playerdata['total'][$oldwinner]['lws_wonlastgame'] = true;
			if($playerdata['total'][$oldwinner]['lws_current'] > $windata['lws_highest']) {
				$playerdata['total'][$oldwinner]['lws_highest'] = $playerdata['total'][$oldwinner]['lws_current'];
			}
		} else {
			if($windata['lws_wonlastgame']) {
				if($windata['lws_current'] > $windata['lws_highest']) {
					$playerdata['total'][$oldwinner]['lws_highest'] = $windata['lws_current'];
				}
				$playerdata['total'][$oldwinner]['lws_current'] = 0;
				$playerdata['total'][$oldwinner]['lws_wonlastgame'] = false;
			}
		}
	}
	$line = trim(fgets($fp));
}
if(count($years) == 1)
	die("No data collected.");
rsort($years);
if(!isset($_POST['year']))
	$year = $years[0];
else
	foreach($years as $year) {
		if($_POST['year'] === $year)
			break;
		else
			continue;
		$year = $years[0];
	}
if(!isset($_POST['sort']))
	$sortby = $sortorders[0];
else
	foreach($sortorders as $sortby) {
		if($_POST['sort'] === $sortby)
			break;
		else
			continue;
		$sortby = $sortorders[0];
	}
$outorder = array();
$oid = 0;
$pid = count($playerdata[$year]);
while($oid < $pid) {
	$oldmax = 0;
	if($sortby === "name")
		$oldmax = "";
	if($sortby === "netgold") {
		$vals = array();
		foreach($playerdata[$year] as $key => $value)
			$vals[] = $value[$sortby];
		$oldmax = min($vals);
	}
	foreach($playerdata[$year] as $curnick => $player) {
		$already_assigned = false;
		if(count($outorder) > 0) {
			foreach($outorder as $nick) {
				if($nick === $curnick) {
					$already_assigned = true;
					break;
				}
			}
		}
		if($already_assigned)
			continue;
		$curmax = $player[$sortby];
		if($curmax >= $oldmax) {
			$foundnick = $curnick;
			$oldmax = $curmax;
		}
	}
	$outorder[] = $foundnick;
	$oid++;
}
if($sortby === "name")
	sort($outorder);
//echo "Finished reading file.\nWarnings: ".$warnings.":\n".$msgout."\n";
print '<!DOCTYPE html>
<html lang="sk">
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<title>Phoenix :: Poker</title>
<style>
.cursor_pointer {
	cursor: pointer;
}

body {
	background-color: '.$bgcolor.';
	color: '.$fgcolor.';
	margin: 0;
	padding: 2rem;
	text-align: center;
	display: block;
}

table {
	position: relative;
	border-collapse: collapse;
	margin-left: auto;
	margin-right: auto;
}

table.full {
	width: 100%;
}

th, td {
	padding: 0.25rem;
	border: 1px solid '.$fgcolor.';
	text-align: center;
}

td {
	vertical-align: top;
}

td.nick {
	font-weight: bold;
	text-align: left;
	color: '.$nickcolor.';
}

td.nrplayers {
	font-weight: bold;
	text-align: center;
	color: '.$nickcolor.';
}

td.mpstats {
	text-align: left;
}

td.digit {
	text-align: right;
}

th {
	background: '.$titlebg.';
	color: '.$titlefg.';
}

th.title {
	position: sticky;
	top: -1px;
}

th.mpgames {
	width: 15%;
}

th.mplist {
	width: 10%;
}

th.sorted {
	background: '.$sorttitlebg.';
	color: '.$sorttitlefg.';
}

th.blank {
	border-top: none;
	border-left: none;
	background: '.$bgcolor.';
	color: '.$fgcolor.';
}

a:link {
	color: #667296;
}

a:visited {
	color: #667296;
}

a:hover {
	color: #667296;
}

.red {
	color: red;
}

.green {
	color: green;
}
</style>
<script>
function toggleVis(e) {
	target=document.getElementById(e);
	if(target.style.display == "none")
		target.style.display = "table-row";
	else
		target.style.display = "none";
}

function sorttable(by) {
	form = document.getElementById(\'sortform\');
	document.getElementById(\'sortselect\').value = by;
	form.submit();
}

function selectyear(yr) {
	form = document.getElementById(\'sortform\');
	document.getElementById(\'yearselect\').value = yr;
	form.submit();
}
</script>
</head>
<body>
<img src="'.$imagefile.'" alt="Phoenix Talker logo">
<h1>Phoenix - štatistiky kockového pokru</h1>
<form id="sortform" method="POST">
<input type="hidden" id="sortselect" name="sort" value="'.$sortby.'" />
<input type="hidden" id="yearselect" name="year" value="'.$year.'" />
</form>
<table>
<tr>';
foreach($years as $value) {
	if($value === "total")
		$out = "všetky roky";
	else
		$out = $value;
	print '<th class="title'.($year == $value ? ' sorted' : ' cursor_pointer').'"'.($year != $value ? ' onclick="selectyear(\''.$value.'\');"' : '').'>'.$out.'</th>
';
}
print '</tr>
</table>
<br/>
<table>
<tr>
<th class="title">Por.</th>
';
foreach($tabletitles as $key => $value)
	print '<th class="title'.($sortby == $key ? ' sorted' : ' cursor_pointer').'"'.($sortby != $key ? ' onclick="sorttable(\''.$key.'\');"' : '').'>'.$value.'</th>
';
print '</tr>
';
$ord = 0;
foreach($outorder as $plname) {
	$ord++;
	print '<tr class="cursor_pointer" onclick="toggleVis(\''.$plname.'_mpstats\')">
<td>'.$ord.'.</td>
<td class="nick">'.$plname.'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['wins'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['lost'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['games'], 0).'</td>
<td class="digit">'.($playerdata[$year][$plname]['topscore'] == 0 ? "n/a" : nrfrmt($playerdata[$year][$plname]['topscore'], 0)).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['wingold'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['lostgold'], 0).'</td>
<td class="digit '.posnegcolor($playerdata[$year][$plname]['netgold']).'">'.nrfrmt($playerdata[$year][$plname]['netgold'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['topwin'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['topbet'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['bets'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['lws_highest'], 0).'</td>
</tr>
<tr id="'.$plname.'_mpstats" style="display: none;">
<td class="mpstats" colspan="13">
<table class="full">
<tr>
<th class="title">Hráči</th>
<th class="title mpgames">Výhry</th>
<th class="title mpgames">Prehry</th>
<th class="title mpgames">Počet hier</th>
<th class="title mpgames">Vyhratý gold</th>
<th class="title mpgames">Prehratý gold</th>
<th class="title mpgames">Zisk / strata</th>
</tr>
';
	for($i = 2; $i <= $games[$year]['maxplayers']; $i++)
		if(isset($playerdata[$year][$plname]['multi'][$i])) {
			print '<tr style="cursor:pointer" onclick="toggleVis(\''.$plname.'_'.$i.'\');">
<td class="nrplayers">'.$i.'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['multi'][$i]['won'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['multi'][$i]['lost'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['multi'][$i]['played'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['multi'][$i]['goldwon'], 0).'</td>
<td class="digit">'.nrfrmt($playerdata[$year][$plname]['multi'][$i]['goldlost'], 0).'</td>
<td class="digit '.posnegcolor($playerdata[$year][$plname]['multi'][$i]['goldnet']).'">'.nrfrmt($playerdata[$year][$plname]['multi'][$i]['goldnet'], 0).'</td>
</tr>
<tr id="'.$plname.'_'.$i.'" style="display: none;">
<td colspan="7">
<table class="full">
<tr>
<th class="title">Spoluhráč/i</th>
<th class="title mplist">Výhry</th>
<th class="title mplist">Prehry</th>
<th class="title mplist">Počet hier</th>
<th class="title mplist">Vyhratý gold</th>
<th class="title mplist">Prehratý gold</th>
<th class="title mplist">Zisk / strata</th>
</tr>
';
			foreach($playerdata[$year][$plname]['groups'][$i] as $players => $playedgames) {
				$played = implode(" + ", explode("|", $players));
				print '<tr>
<td class="nick">'.$played.'</td>
<td class="digit">'.nrfrmt($playedgames['wins'], 0).'</td>
<td class="digit">'.nrfrmt($playedgames['lost'], 0).'</td>
<td class="digit">'.nrfrmt($playedgames['plays'], 0).'</td>
<td class="digit">'.nrfrmt($playedgames['goldwon'], 0).'</td>
<td class="digit">'.nrfrmt($playedgames['goldlost'], 0).'</td>
<td class="digit '.posnegcolor($playedgames['goldnet']).'">'.nrfrmt($playedgames['goldnet'], 0).'</td>
</tr>
';
			}
			print '</table>
</td>
</tr>
';
		}
	print '</table>
</td>
</tr>
';
}
print '</table>
';
$topbetplayers = "";
foreach($games[$year]['topbetplayers'] as $name)
	if($topbetplayers == "")
		$topbetplayers = $name;
	else
		$topbetplayers.= ", ".$name;
$topwinplayers = "";
foreach($games[$year]['topwinplayers'] as $name)
	if($topwinplayers == "")
		$topwinplayers = $name;
	else
		$topwinplayers.= ", ".$name;
print '<h2>Celkové údaje</h2>
<table>
<tr><th>Počet hier</th><td class="digit">'.nrfrmt($games[$year]['count'], 0).'</td></tr>
<tr><th>Celkový gold</th><td class="digit">'.nrfrmt($games[$year]['gold'], 0).'</td></tr>
<tr><th>Najvyššie víťazné skóre<br/>(hráč)</th><td class="digit">'.nrfrmt($games[$year]['topscore'], 0).'<br/>('.$games[$year]['topscoreplayer'].')</td></tr>
<tr><th>Najvyššia stávka<br/>(hráč/i)</th><td class="digit">'.nrfrmt($games[$year]['topbet'], 0).'<br/>('.$topbetplayers.')</td></tr>
<tr><th>Najvyššia výhra<br/>(hráč/i)</th><td class="digit">'.nrfrmt($games[$year]['topwin'], 0).'<br/>('.$topwinplayers.')</td></tr>
<tr><th>Maximálny počet hráčov</th><td class="digit">'.nrfrmt($games[$year]['maxplayers'], 0).'</td></tr>
<tr>
<th>Hra s najvyšším bankom</th>
<td>
<table>
<tr>
<th>Bank</th>
<th>Dátum a čas</th>
<th>Hráči (stávky)</th>
<th>Víťaz</th>
<th>Výhra</th>
</tr>
<tr>
<td class="digit">'.nrfrmt($games[$year]['highestpot']['pot'], 0).'</td>
<td>'.$games[$year]['highestpot']['date'].' '.$games[$year]['highestpot']['time'].'</td>
<td>';
$out = "";
foreach($games[$year]['highestpot']['players'] as $plname => $bet)
	if($out == "")
		$out.= $plname.'&nbsp;('.nrfrmt($bet, 0).')';
	else
		$out.= '<br/>'.$plname.'&nbsp;('.nrfrmt($bet, 0).')';
print $out.'</td>
<td>'.$games[$year]['highestpot']['winner'].'</td>
<td class="digit">'.nrfrmt($games[$year]['highestpot']['win'], 0).'</td>
</tr>
</table>
</td>
</tr>
</table>
';
if($year != "total") {
	print '<h2>Údaje po mesiacoch</h2>
		<table>
<tr>
<th class="blank"></th>
';
	foreach($games[$year]['months'] as $month => $data)
		print '<th class="title">'.$monthnames[(int)$month-1].'</th>
';
	print '</tr>
<tr>
<th>Počet hier</th>
';
	foreach($games[$year]['months'] as $month => $data)
		print '<td class="digit">'.nrfrmt($data['count'], 0).'</td>
';
	print '</tr>
<tr>
<th>Celkový gold</th>
';
	foreach($games[$year]['months'] as $month => $data)
		print '<td class="digit">'.nrfrmt($data['gold'], 0).'</td>
';
	print '</tr>
<tr>
<th>Najvyššie víťazné skóre<br/>(hráč)</th>
';
	foreach($games[$year]['months'] as $month => $data)
		print '<td class="digit">'.nrfrmt($data['topscore'], 0).'<br/>('.$data['topscoreplayer'].')</td>
';
	print '</tr>
<tr>
<th>Najvyššia stávka<br/>(hráč/i)</th>
';
	foreach($games[$year]['months'] as $month => $data) {
		$topbetplayers = "";
		foreach($data['topbetplayers'] as $name)
			if($topbetplayers == "")
				$topbetplayers = $name;
			else
				$topbetplayers.= ",<br/>".$name;
		print '<td class="digit">'.nrfrmt($data['topbet'], 0).'<br/>('.$topbetplayers.')</td>
';
	}
	print '</tr>
<tr>
<th>Najvyššia výhra<br/>(hráč/i)</th>
';
	foreach($games[$year]['months'] as $month => $data) {
		$topwinplayers = "";
		foreach($data['topwinplayers'] as $name)
			if($topwinplayers == "")
				$topwinplayers = $name;
			else
				$topwinplayers.= ",<br/>".$name;
		print '<td class="digit">'.nrfrmt($data['topwin'], 0).'<br/>('.$topwinplayers.')</td>
';
	}
	print '</tr>
<tr>
<th>Maximálny počet hráčov</th>
';
	foreach($games[$year]['months'] as $month => $data)
		print '<td class="digit">'.nrfrmt($data['maxplayers'], 0).'</td>
';
	print '</tr>
</table>
<h3>Hry s najvyšším bankom</h3>
<table>
<tr>
<th class="title">Mesiac</th>
<th class="title">Bank</th>
<th class="title">Dátum a čas</th>
<th class="title">Hráči (stávky)</th>
<th class="title">Víťaz</th>
<th class="title">Výhra</th>
</tr>
';
	foreach($games[$year]['months'] as $month => $data) {
		print '<tr>
<th>'.$monthnames[(int)$month-1].'</th>
<td class="digit">'.nrfrmt($data['highestpot']['pot'], 0).'</td>
<td>'.$data['highestpot']['date'].' '.$data['highestpot']['time'].'</td>
<td>';
	$out = "";
	foreach($data['highestpot']['players'] as $plname => $bet)
		if($out == "")
			$out.= $plname.'&nbsp;('.nrfrmt($bet, 0).')';
		else
			$out.= '<br/>'.$plname.'&nbsp;('.nrfrmt($bet, 0).')';
			print $out.'</td>
<td>'.$data['highestpot']['winner'].'</td>
<td class="digit">'.nrfrmt($data['highestpot']['win'], 0).'</td>
</tr>
';
	}
	print '</table>
';
}
print '<br/>
<a href="./">Späť na hlavnú stránku</a>
<br/>
<br/>
<a href="'.$src_url.'" target="_blank">Štatistiku pre Phoenix talker nakódil Voodoo</a><br/>
</body>
</html>';
