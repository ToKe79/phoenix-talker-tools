<?php
header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
header("Pragma: no-cache"); // HTTP 1.0.
header("Expires: 0"); // Proxies.

$load = "";
$save = "";

$meno = "";
$heslo = "";
$profil = "";
$action = "";
$max_newlines = 30;
$max_profile_size = 10240;

$cp_web = "UTF-8";
$cp_talker = "ASCII";
setlocale(LC_ALL, "sk_SK.UTF-8");

$path = "/home/loomy/nuts/userfiles";

if(isset($_POST['meno']))
	$meno = $_POST['meno'];
if(isset($_POST['heslo']))
	$heslo = $_POST['heslo'];
if(isset($_POST['profil']))
	$profil = $_POST['profil'];
if(isset($_POST['action']))
	$action = $_POST['action'];

// body colors
$bgcolor = "black";
$fgcolor = "#5e84a5";

$cssdata = 'body {
	background-color: '.$bgcolor.';
	color: '.$fgcolor.';
	margin: 0;
	padding: 2rem;
	text-align: center;
	display: block;
}

a:link {
	color: #667296;
}

a:visited {
	color: #667296;
}

a:hover {
	color: #667296;
}';

$jsdata = '';

$htmlstart = '<!DOCTYPE html>
<html lang="sk">
<head>
<meta http-equiv="Content-Type" content="text/html;charset='.$cp_web.'">
<title>Phoenix :: Profil editor</title>
<style>
'.$cssdata.'
</style>
<script>
'.$jsdata.'
</script>
</head>
<body>
<h1>Phoenix - editor profilu</h1>';

$htmlend = '<br/>
<a href="./">Späť na hlavnú stránku</a>
</body>
</html>';

function profileform($message, $meno, $heslo, $profil) {
	global $htmlstart, $htmlend, $max_profile_size, $max_newlines;

	print $htmlstart.'
<p>'.$message.'</p>
<form method="post">
<textarea name="profil" maxlength="'.$max_profile_size.'" rows="'.$max_newlines.'" cols="80">'.htmlentities($profil).'</textarea><br>
<input type="text" name="meno" placeholder="Meno" value="'.htmlentities($meno).'" maxlength="12"><br>
<input type="password" name="heslo" placeholder="Heslo" value="'.htmlentities($heslo).'" maxlength="20"><br>
<input type="hidden" name="action" value="save_profile">
<input type="submit" value="Ulož">
</form>
'.$htmlend;
}

function sklon($count, $five, $two, $one) {
	if($count > 4 || $count == 0)
		return $five;
	elseif ($count > 1)
		return $two;
	else
		return $one;
}

function intro($message, $meno, $heslo) {
	global $htmlstart, $htmlend;

	print $htmlstart.'
<p>'.$message.'</p>
<form method="post">
<input type="text" name="meno" placeholder="Meno" value="'.htmlentities($meno).'" maxlength="12"><br>
<input type="password" name="heslo" placeholder="Heslo" value="'.htmlentities($heslo).'" maxlength="20"><br>
<input type="hidden" name="action" value="load_profile">
<input type="submit" value="Prihlás">
</form>
'.$htmlend;
}

function checkpwd ($heslo, $hash) {
	if(strlen($hash)<15) {
		if(strcmp($hash, crypt($heslo, 'NU')) == 0)
			return 0;
		else
			return 1;
	}
	else {
		$prvychosem = substr($heslo, 0, 8);
		$druhychosem = substr($heslo, 8, 8);
		$dokopy = crypt($prvychosem, 'NU') + crypt($druhychosem, 'NU');
		if($dokopy == $hash)
			return 0;
		else
			return 1;
	}
}

if($action == "save_profile") {
	if($meno != "" && $heslo != "" && $profil != "") {
		$profile_newlines = substr_count($profil, "\n");
		if(substr($profil, -1) != "\n")
			$profile_newlines++;
		if($profile_newlines <= $max_newlines) {
			$profile_size = strlen($profil);
			if($profile_size <= $max_profile_size) {
				$nick = strtoupper($meno[0]).substr($meno, 1);
				$file = $path."/".$nick.".D";
				if(file_exists($file)) {
					$f = fopen($file, "r");
					$line = fgets($f);
					fclose($f);
					$l = str_replace("\n", "", $line);
					if(checkpwd($heslo, $l) == 0) {
						if(substr($profil, -1) != "\n")
							$profil.= "\n";
						$file = $path."/".$nick.".P";
						$fp = fopen($file, "w") or die("ERROR: could not access file for writing!");
						fwrite($fp, $profil) or die("ERROR: could not write to file!");
						fclose($fp);
						$save = "success";
					}
					else
						$save = "wrongPass";
				}
				else
					$save = "wrongPass";
			}
			else
				$save = "tooBig";
		}
		else
			$save = "tooManyLines";
		switch($save) {
		case "success":
			$message = "<span style=\"color:green;\">Tvoj profil bol uložený.</span>";
			break;
		case "tooManyLines":
			$over = $profile_newlines - $max_newlines;
//			$message = "Profil môže mať maximálne ".$max_newlines." riadkov! Skráť ho o ".$over." riadkov.";
			$message = sprintf("<span style=\"color:red;\">Profil môže mať maximálne %d riad%s! Skráť ho o %d riad%s.</span>",
				$max_newlines,
				sklon($max_newlines, "kov", "ky", "ok"),
				$over,
				sklon($over, "kov", "ky", "ok"));
			break;
		case "tooBig":
			$over = $profile_size - $max_profile_size;
			$message = sprintf("<span style=\"color:red;\">Profil môže mať maximálne %d byt%! Skráť ho o %d byt%s.</span>",
				$max_profile_size,
				sklon($max_profile_size, "ov", "y", "e"),
				$over,
				sklon($over, "ov", "y", "e"));
			break;
		case "wrongPass":
			$message = "<span style=\"color:red;\">Nesprávne meno alebo heslo!</span>";
			break;
		}
	}
	else
		$message = "<span style=\"color:red;\">Na uloženie profilu treba zadať meno, heslo a vyplniť profil!</span>";
	profileform($message, $meno, $heslo, $profil);
	die();
}
elseif($action == "load_profile") {
	if($meno != "" && $heslo != "") {
		if(strlen($meno) < 3)
			$load = "nickTooShort";
		else {
			$nick = strtoupper($meno[0]).substr($meno, 1);
			$file = $path."/".$nick.".D";
			if(file_exists($file)) {
				$f = fopen($file, "r");
				$line = fgets($f);
				fclose($f);
				$l = str_replace("\n", "", $line);
				if(checkpwd($heslo, $l) == 0) {
					$file = $path."/".$nick.".P";
					if(file_exists($file))
						$profil = file_get_contents($file);
					else
						$profil = "(Prazdny profil)";
					$load = "success";
				}
				else
					$load = "wrongPass";
			}
			else
				$load = "wrongPass";
		}
		switch($load) {
		case "success":
			$message = "<span style=\"color:green;\">Profil načítaný.</span>";
			profileform($message, $meno, $heslo, $profil);
			die();
			break;
		case "nickTooShort":
		case "wrongPass":
			$message = "<span style=\"color:red;\">Nesprávne meno alebo heslo!</span>";
			break;
		}
	}
	else
		$message = "<span style=\"color:red;\">Na načítanie profilu treba zadať meno a heslo!</span>";
}
else
	$message = "Prihlásenie tu ťa neodhlási z talkera.";
intro($message, $meno, $heslo);
